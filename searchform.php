<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <div class="row collapse postfix-round">
        <div class="small-9 columns">
            <input type="text" name="s" id="s" placeholder="Typ wat u zoekt...">
        </div>
        <div class="small-3 columns">
            <button type="submit" class="postfix expand"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>