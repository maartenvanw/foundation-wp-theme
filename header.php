<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php bloginfo('title') ?></title>
    <?php wp_head(); ?>
  </head>
<body <?php body_class(); ?>>
    <header>
        <div class="row">
            <?php wp_nav_menu(array('theme_location' => 'top-menu', 'container' => 'nav', 'container_class' => 'menu')); ?>
        </div>
    </header>
    
    <div class="row"> 