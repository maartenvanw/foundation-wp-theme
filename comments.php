<div class="comments">
    <?php if ( have_comments() ) {
        ?>
        <h3><?php comments_number('Geen reacties', '1 Reactie', '% Reacties'); ?></h3>
            <ul class="commentlist">
                <?php $commentListArgs = array(
                        'callback'      => 'owntheme_comment',
                        'avatar_size'   => 64,
                        'reply_text'    => '<div class="text-right">Beantwoord <i class="fa fa-reply"></i></div>'
                    ) ?>
                <?php wp_list_comments($commentListArgs); ?>
            </ul>
    <? } ?>

    <?php 
        
        $commentsArgs = array(
                'label_submit'          => 'Plaats Reactie',
                'title_reply'           => 'Plaats een reactie',
                'comment_notes_after'   => ''
            ); 
            
    ?>
    <?php comment_form($commentsArgs); ?>
</div>
