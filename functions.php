<?php 

/**
 * 
 * Scripts loading
 * 
 */

function owntheme_script_enqueue() {
    
    wp_enqueue_style('foundation-style', get_template_directory_uri() . '/css/app.css', 'all');

    wp_enqueue_script('modernizer-js', get_template_directory_uri() . '/bower_components/modernizr/modernizr.js', false);

    wp_enqueue_script('jqeury-lib', get_template_directory_uri() . '/bower_components/jquery/dist/jquery.min.js', false, false, true);

    wp_enqueue_script('foundation-js', get_template_directory_uri() . '/bower_components/foundation/js/foundation.min.js', false, false, true);

    wp_enqueue_script('custom-js', get_template_directory_uri() . '/js/app.js', false, false, true);
    
}

add_action( 'wp_enqueue_scripts', 'owntheme_script_enqueue');

/**
 * 
 * Theme setup
 * 
 */

function owntheme_theme_setup() {
    add_theme_support('menus');
    
    register_nav_menu('top-menu', 'Top menu');
    
}

add_action('init' ,'owntheme_theme_setup');

add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
add_theme_support('post-formats', array('aside', 'image','video','gallery','audio'));

/**
 * 
 * Widget setup
 * 
 */

function owntheme_widget_setup() {
    register_sidebar(
    array(
        'name'          => 'Sidebar',
        'id'            => 'sidebar',
        'description'   => 'Right sidebar',
        'before_widget' => '<div class="content-header">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2></div><div class="content">',
        )
    );
}

add_action('widgets_init', 'owntheme_widget_setup');

/**
 * 
 * Comments style
 * 
 */

function owntheme_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
?>
    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body clearfix">
    <?php endif; ?>
    <div class="wrapper">
        <div class="comment-author">
        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
        </div>
        <div class="extra-wrap">
            <?php printf( __( '<span class="author">%s</span>' ), get_comment_author_link() ); ?>
            <br>
            <?php
                /* translators: 1: date, 2: time */
                printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
            ?>
            <?php comment_text(); ?>
        </div>
    </div>
    <div class="wraper">
        <div class="reply">
            <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
        </div>
    </div>
    <?php if ( $comment->comment_approved == '0' ) : ?>
        <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
        <br />
    <?php endif; ?>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
<?php
}

/**
 * 
 * Pagination
 * 
 */


function new_centered_pagination($pages = '', $range = 2)
{
     $showitems = ($range * 2)+1;
     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }
     if(1 != $pages)
     {
         echo '<div class="pagination-centered"><ul class="pagination">';
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li class='arrow'><a href='".get_pagenum_link(1)."'>«</a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li class='arrow'><a href='".get_pagenum_link($paged - 1)."'>«</a></li>";
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class='current'><a href=''>".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
             }
         }
         if ($paged < $pages && $showitems < $pages) echo "<li class='arrow'><a href='".get_pagenum_link($paged + 1)."'>»</a></li>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li class='arrow'><a href='".get_pagenum_link($pages)."'>»</a></li>";
         echo "</ul></div>";
     }
}

/**
 * 
 * Meta data / Posted on
 * 
 */

function twentyfourteen_posted_on() {
    if ( is_sticky() && is_home() && ! is_paged() ) {
        echo '<span class="featured-post">' . __( 'Sticky', 'twentyfourteen' ) . '</span>';
    }

    // Set up and print post meta information.
    printf( '<i class="fa fa-clock-o"></i>&nbsp;&nbsp;<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a></span> <span class="byline"><i class="fa fa-user"></i>&nbsp;&nbsp;<span class="author"><a class="url fn n" href="%4$s" rel="author">%5$s</a></span></span>',
        esc_url( get_permalink() ),
        esc_attr( get_the_date( 'c' ) ),
        esc_html( get_the_date() ),
        esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
        get_the_author()
    );
}

/**
 * 
 * Custom post format options
 * 
 */

function owntheme_post_format_options() {
    
    $screens = array( 'post' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'video',
            __( 'Video Link', 'myplugin_textdomain' ),
            'owntheme_meta_box_callback',
            $screen
        );
    }
    
}

add_action( 'add_meta_boxes', 'owntheme_post_format_options' );

function owntheme_meta_box_callback( $post ) {

    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'myplugin_meta_box', 'myplugin_meta_box_nonce' );

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $value = get_post_meta( $post->ID, '_my_meta_value_key', true );

    echo '<label for="video">';
    _e( 'Plaats video link', 'myplugin_textdomain' );
    echo '</label> ';
    echo '<input type="text" id="video" name="video" value="' . esc_attr( $value ) . '" />';
}

function myplugin_save_meta_box_data( $post_id ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( ! isset( $_POST['myplugin_meta_box_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['myplugin_meta_box_nonce'], 'myplugin_meta_box' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */
    
    // Make sure that it is set.
    if ( ! isset( $_POST['myplugin_new_field'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['myplugin_new_field'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_my_meta_value_key', $my_data );
}
add_action( 'save_post', 'myplugin_save_meta_box_data' );