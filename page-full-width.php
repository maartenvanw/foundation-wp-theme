<?php get_header(); ?>
    
    <div class="small-12 end columns">
        <?php if( have_posts() ) { while( have_posts() ){ the_post(); ?> 
            
            <div class="content-header">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="content-author">
                Geplaatst op <?php the_time('j F, Y'); ?> <?php the_time('g:i') ?> bij <?php echo get_the_author_link(); ?> 
            </div>
            <div class="content">
                <p><?php the_content(); ?></p>
            </div>
            <?php echo edit_post_link('Bewerk <i class="fa fa-pencil"></i>', '<div class="content-edit"><p class="text-right">', '</p></div>'); ?>
                
        <?php } } ?>
    </div>
    
<?php get_footer(); ?>