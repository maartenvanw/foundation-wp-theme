<?php get_header(); ?>
    
    <div class="small-12 medium-8 end columns">
        <?php if( have_posts() ) { while( have_posts() ){ the_post(); ?> 
            
            <?php get_template_part('content', get_post_format()); ?>
                
        <?php } } ?>
        <?php $lastBlog = new WP_Query('type=post&posts_per_page=1'); ?>
        <?php if( $lastBlog->have_posts() ) { while( $lastBlog->have_posts() ){ $lastBlog->the_post(); ?> 
        
            <?php get_template_part('content', get_post_format()); ?>
                
        <?php } } ?>
    </div>
    <?php get_sidebar(); ?>
<?php get_footer(); ?>