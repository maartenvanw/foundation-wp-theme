<?php get_header(); ?>
    
    <div class="small-12 medium-8 end columns">
        <?php if( have_posts() ) { while( have_posts() ){ the_post(); ?> 
            
            <?php get_template_part('content', 'search'); ?>
            
        <?php } } else {
                get_template_part( 'content', 'none' );
            } ?>
    </div>
    <?php get_sidebar(); ?>
<?php get_footer(); ?>