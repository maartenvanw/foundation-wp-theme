<div class="content-container">
    <div class="content-header">
        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </div>
    <div class="content-author">
        Geplaatst op <?php the_time('j F, Y'); ?> <?php the_time('g:i') ?> bij <?php echo get_the_author_link(); ?> 
    </div>
    <div class="content clearfix">
        <p><?php the_content( 'Lees meer' , false ); ?></p>
    </div>
    <?php echo edit_post_link('Bewerk <i class="fa fa-pencil"></i>', '<div class="content-edit"><p class="text-right">', '</p></div>'); ?>
</div>
