<?php 
/*
    Template Name: No title 
 */

get_header(); ?>
    
    <div class="small-12 medium-8 end columns">
        <?php if( have_posts() ) { while( have_posts() ){ the_post(); ?> 
            
           <?php if ( has_post_thumbnail() ) { ?>
                
                <div class="content-image-no-title">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="content">
                    <p><?php the_content(); ?></p>
                </div>
                <?php echo edit_post_link('Bewerk <i class="fa fa-pencil"></i>', '<div class="content-edit"><p class="text-right">', '</p></div>'); ?>
            <?php } else { ?>
                <div class="content-no-title">
                    <p><?php the_content( 'Lees meer' , false ); ?></p>
                </div>
                <?php echo edit_post_link('Bewerk <i class="fa fa-pencil"></i>', '<div class="content-edit"><p class="text-right">', '</p></div>'); ?>
            <?php } ?>
                
        <?php } } ?>
    </div>
    
<?php get_footer(); ?>